import { Component, OnInit } from '@angular/core';
import { ChimisService, Experience } from '../../services/chimis.service';

@Component({
  selector: 'app-chimis',
  templateUrl: './chimis.component.html',
  styles: []
})
export class ChimisComponent implements OnInit {

  experiences: Experience[] = [];

  constructor(
    private _chimisService: ChimisService
  ) { }

  ngOnInit() {
    this.experiences = this._chimisService.getExperiences();
    console.log(this.experiences)
  }

}
