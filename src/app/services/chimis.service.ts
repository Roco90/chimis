import { Injectable } from '@angular/core'

@Injectable()
export class ChimisService {

    private experiences: Experience[] = [
        {
          title: "One",
          description: "Desc1",
          word: "1",
          img: "assets/img/Cas.JPG"
        },
        {
          title: "Two",
          description: "Desc2",
          word: "2",
          img: "assets/img/Aca.jpg"
        },
        {
          title: "Tree",
          description: "Desc3",
          word: "3",
          img: "assets/img/Aju.jpg"
        },
        {
          title: "Four",
          description: "Desc4",
          word: "4",
          img: "assets/img/Cinco.jpg"
        },
        {
          title: "Five",
          description: "Desc5",
          word: "5",
          img: "assets/img/Cue.jpg"
        },
        {
          title: "Six",
          description: "Desc6",
          word: "6",
          img: "assets/img/Dar.JPG"
        },
        {
          title: "Seven",
          description: "Desc7",
          word: "7",
          img: "assets/img/Ris.jpg"
        },
        {
          title: "Eight",
          description: "Desc8",
          word: "8",
          img: "assets/img/Teo.jpg"
        },
        {
          title: "Nine",
          description: "Desc9",
          word: "9",
          img: "assets/img/Bes.JPG"
        },
        {
          title: "Ten",
          description: "Desc10",
          word: "10",
          img: "assets/img/Aut.jpg"
        },
        {
          title: "Eleven",
          description: "Desc11",
          word: "11",
          img: "assets/img/Pas.jpg"
        },
        {
          title: "Twelve",
          description: "Desc12",
          word: "12",
          img: "assets/img/Hom.jpg"
        },
        {
          title: "Thirteen",
          description: "Desc13",
          word: "13",
          img: "assets/img/Fot.jpg"
        },
        {
          title: "Fourteen",
          description: "Desc14",
          word: "14",
          img: "assets/img/Abr.jpg"
        },
        {
          title: "Fiftteen",
          description: "Desc15",
          word: "15",
          img: "assets/img/Eje.jpg"
        },
      ];

    constructor() {
        
    }

    getExperiences() {
        return this.experiences;
      }
}

export interface Experience {
    title: string;
    description: string;
    word: string;
    img: string;
  }