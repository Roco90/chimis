import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ChimisComponent } from './components/chimis/chimis.component';

export const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent},
    { path: 'about', component: AboutComponent},
    { path: 'chimis', component: ChimisComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'home'}
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
// export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true});
